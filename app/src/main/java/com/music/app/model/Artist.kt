package com.music.app.model

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import kotlinx.android.parcel.Parcelize
import java.util.ArrayList

@JsonObject
class Artist (

    @JsonField(name = ["id"])
    var id: String? = null,

    @JsonField(name = ["images"])
    var images: ArrayList<Image> ?= null,

    @JsonField(name = ["genres"])
    var genres: ArrayList<String> ?= null,

    @JsonField(name = ["name"])
    var name: String? = null,

    @JsonField(name = ["href"])
    var href: String? = null,

    @JsonField(name = ["popularity"])
    var popularity: Int? = null,

    @JsonField(name = ["type"])
    var type: String? = null

):BaseObservable(){

    @Bindable
    fun getCover(): String? {

        if(!images.isNullOrEmpty()){
            return images?.get(0)?.url
        }
        return ""
    }

    val popularityText: String

        get() = "Popularity: "+popularity

    @Bindable
    fun getGenresText(): String {

        val text = StringBuilder()

        genres?.let {

            if (it.isNotEmpty()) {

                for (i in it.indices) {
                    if (i != 0) {
                        text.append(" , ")
                    }
                    text.append(it[i])
                }
            }
        }

        return "Genre: " +text.toString()

    }
}