package com.music.app.model

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class Image(

    @JsonField(name = ["height"])
    var height: Int? = null,

    @JsonField(name = ["url"])
    var url: String? = null,

    @JsonField(name = ["width"])
    var width: Int? = null
)