package com.music.app.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import kotlinx.android.parcel.Parcelize


@Entity(tableName = "users")
@JsonObject
open class User(

    @PrimaryKey
    @JsonField(name = ["Id"])
    var id: Int? = null,

    @ColumnInfo
    @JsonField(name = ["FirstName"])
    var firstName: String? = null,

    @ColumnInfo
    @JsonField(name = ["LastName"])
    var lastName: String? = null,

    @ColumnInfo
    @JsonField(name = ["Email"])
    var email: String? = null,

    @ColumnInfo
    @JsonField(name = ["UserName"])
    var userName: String? = null,

    @ColumnInfo
    @JsonField(name = ["PhoneNo"])
    var phone: String? = null,

    @Ignore
    @JsonField(name = ["Token"])
    var token: String? = null,

    @ColumnInfo
    @JsonField(name = ["AvatarUrl"])
    var avatar: String? = null,

    @Ignore
    @JsonField(name = ["InviteCode"])
    var inviteCode: String? = null

)