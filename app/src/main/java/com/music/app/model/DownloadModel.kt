package com.music.app.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "downloadFiles")
class DownloadModel {

    @PrimaryKey
    @ColumnInfo
    var contentId: Int = -1
    @ColumnInfo
    var downloadId: Int = -1
    @ColumnInfo
    var zoneId: Int = -1
    @ColumnInfo
    var qualityId: Int = -1
    @ColumnInfo
    var contentName: String? = null
    @ColumnInfo
    var contentImage: String? = null
    @ColumnInfo
    var contentPath: String? = null
    @ColumnInfo
    var contentSize: Long = 0
    @ColumnInfo
    var downloaded: Long = 0
    @ColumnInfo
    var downloadStatus: Int = UNKNOWN
    @ColumnInfo
    var downloadProgress: Int = 0
    @ColumnInfo
    var type: Int = -1
    @ColumnInfo
    var subtitle: String? = null


    companion object {

        const val UNKNOWN = -1
        const val QUEUED = 0
        const val DOWNLOADING = 1
        const val PAUSED = 2
        const val COMPLETED = 3
        const val FAILED = 4
    }
}