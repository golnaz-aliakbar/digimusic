package com.music.app.model

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject
import java.util.ArrayList

@JsonObject
class SearchResponse(

    @JsonField(name = ["items"])
    var artists: ArrayList<Artist>? = null,

    @JsonField(name = ["limit"])
    var limit: Int? = null,

    @JsonField(name = ["next"])
    var next: Int? = null,

    @JsonField(name = ["offset"])
    var offset: Int? = null,

    @JsonField(name = ["previous"])
    var previous: Int? = null,

    @JsonField(name = ["total"])
    var total: Int? = null,

    @JsonField(name = ["href"])
    var href: Int? = null

    )