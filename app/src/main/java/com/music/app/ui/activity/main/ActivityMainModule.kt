package com.music.app.ui.activity.main

import com.music.app.di.modules.data.DataManager
import com.music.app.di.modules.data.scheduler.SchedulersFacade
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class ActivityMainModule {
    @Provides
    fun provideViewModel(
        dataManager: DataManager,
        compositeDisposable: CompositeDisposable, schedulersFacade: SchedulersFacade
    ): ActivityMainViewModel {
        return ActivityMainViewModel(dataManager, compositeDisposable, schedulersFacade)
    }
}