package com.music.app.ui.activity.detail

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.music.app.R
import com.music.app.databinding.ActivityDetailBinding
import com.music.app.di.InjectionViewModelProvider
import com.music.app.helper.progresshandler.ViewProgressHandler
import com.music.app.ui.base.BaseActivity
import javax.inject.Inject

class ActivityDetail : BaseActivity<ActivityDetailBinding, ActivityDetailViewModel>() {

    @Inject
    lateinit var mViewModelFactoryActivity: InjectionViewModelProvider<ActivityDetailViewModel>

    override val bindingVariable: Int? = null
    override fun getLayoutId() = R.layout.activity_detail


    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = mViewModelFactoryActivity.get(this, ActivityDetailViewModel::class)

        init()

        viewModel?.artistResult?.observe(this, Observer {

            binding.vm = it
        })

        viewModel?.getArtist(intent.getStringExtra(EXTRA_ARTIST_ID))


    }

    private fun init() {


        viewModel?.loading = ViewProgressHandler(
            {
                binding.progressBar.visibility = View.VISIBLE
            },
            {
                binding.progressBar.visibility = View.INVISIBLE
            }
        )

    }

    companion object {
        const val EXTRA_ARTIST_ID = "EXTRA_ARTIST_ID"
        fun navigate(context: Context,id: String) {
            val i = Intent(context, ActivityDetail::class.java)
            i.putExtra(EXTRA_ARTIST_ID,id)
            context.startActivity(i)
        }
    }

}