package com.music.app.ui.activity.detail

import com.music.app.di.modules.data.DataManager
import com.music.app.di.modules.data.scheduler.SchedulersFacade
import com.music.app.ui.activity.main.ActivityMainViewModel
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable


@Module
class ActivityDetailModule {
    @Provides
    fun provideViewModel(
        dataManager: DataManager,
        compositeDisposable: CompositeDisposable, schedulersFacade: SchedulersFacade
    ): ActivityDetailViewModel {
        return ActivityDetailViewModel(dataManager, compositeDisposable, schedulersFacade)
    }
}