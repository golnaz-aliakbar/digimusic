package com.music.app.ui.activity.login

import com.music.app.di.modules.data.DataManager
import com.music.app.di.modules.data.account.AccountManager
import com.music.app.di.modules.data.pref.PreferencesManager
import com.music.app.di.modules.data.scheduler.SchedulersFacade
import com.music.app.ui.base.BaseViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.HashMap

class ActivityLoginViewModel (dataManager: DataManager,
                              compositeDisposable: CompositeDisposable,
                              schedulersFacade: SchedulersFacade
) : BaseViewModel(dataManager, compositeDisposable, schedulersFacade) {

    private var disposable: HashMap<String, Disposable?> = HashMap()


    fun getSharedPreferences(): PreferencesManager {
        return mDataManager.preferencesManager
    }
    fun getAccountManager(): AccountManager {
        return mDataManager.accountManager
    }

}
