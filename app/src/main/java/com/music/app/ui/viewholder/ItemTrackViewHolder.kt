package com.music.app.ui.viewholder

import android.content.Context
import androidx.databinding.ViewDataBinding
import com.music.app.BR
import com.music.app.ui.adapter.BinderViewHolderParent


class ItemTrackViewHolder(binding: ViewDataBinding, viewType: Int) :
    BinderViewHolderParent(binding, viewType) {

    override fun bindView(context: Context, item: Any?, position: Int) {
        super.bindView(context, item, adapterPosition)
            binding.setVariable(BR.vm, item)

            binding.root.setOnClickListener {
                onClickListener?.invoke(binding.root, adapterPosition)
            }

    }
}