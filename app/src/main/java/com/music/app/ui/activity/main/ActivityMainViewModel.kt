package com.music.app.ui.activity.main

import androidx.lifecycle.MutableLiveData
import com.music.app.di.modules.data.DataManager
import com.music.app.di.modules.data.scheduler.SchedulersFacade
import com.music.app.model.Artist
import com.music.app.model.SearchResponse
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import com.music.app.ui.base.BaseViewModel
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit

class ActivityMainViewModel(
    dataManager: DataManager,
    compositeDisposable: CompositeDisposable,
    schedulersFacade: SchedulersFacade
) : BaseViewModel(dataManager, compositeDisposable, schedulersFacade) {

    private var disposable: HashMap<String, Disposable?> = HashMap()

    var searchResult = MutableLiveData<ArrayList<Artist>>()
    var publishSubject = PublishSubject.create<String>()

    internal fun getSearch(query: String) {
        disposable["0"]?.dispose()
        disposable["0"] =
            publishSubject.debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(mSchedulersFacade.io())
                .observeOn(mSchedulersFacade.ui()).filter {
                        text -> text.isNotEmpty()
                }
                .switchMapSingle {
                    makeRequest(
                        mDataManager.networkManager.getSearchApi().getSearchResult(it, "artist"),
                        SearchResponse::class.java,
                        loading
                    )
                }
                .subscribe({
                    searchResult.value = it.artists
                }, Timber::e)

        addDisposable(disposable["0"]!!)
    }
    fun onQuery(searchTerm: String) {

        publishSubject.onNext(searchTerm)
    }


}
