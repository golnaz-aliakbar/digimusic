package com.music.app.ui.adapter

import androidx.annotation.LayoutRes

class BindingContainer {

    var holder: Class<BinderViewHolderParent>? = null
    var obj: Any? = null
    @LayoutRes
    var layout: Int = 0
        private set

    constructor(layout: Int) {
        this.layout = layout
    }

    constructor(holder: Class<BinderViewHolderParent>, obj: Any?, @LayoutRes layout: Int) {
        this.holder = holder
        this.obj = obj
        this.layout = layout
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is BindingContainer)
            return false
        val container = other as BindingContainer?
        return container?.layout == this.layout
    }
}
