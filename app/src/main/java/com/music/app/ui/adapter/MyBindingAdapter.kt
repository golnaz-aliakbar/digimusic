package com.music.app.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView


class MyBindingAdapter(
    private val context: Context,
    items: List<*>?,
    private var cls: Class<*>,
    @LayoutRes vararg layout: Int,
    private val recyclerItemClickListener: ((view: View, position: Int) -> Unit)? = null,
    private val recyclerItemLongClickListener: ((view: View, position: Int) -> Unit)? = null
) : RecyclerView.Adapter<BinderViewHolderParent>() {


    private var items: ArrayList<Any>? = null
    private var layout: IntArray? = null
    private var selectedHash = -1
//    private var lastPosition = -1

    init {
        this.items = items as ArrayList<Any>
        this.layout = layout
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BinderViewHolderParent {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(context),
            layout?.get(viewType)!!,
            parent,
            false
        )
        var viewHolder: BinderViewHolderParent? = null
        try {
            viewHolder = cls.getConstructor(
                ViewDataBinding::class.java,
                Int::class.javaPrimitiveType
            ).newInstance(binding, viewType) as BinderViewHolderParent
            viewHolder.onClickListener = recyclerItemClickListener
            viewHolder.onLongClickListener = recyclerItemLongClickListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (viewHolder == null) {
            throw RuntimeException("${cls.canonicalName} not found")
        }
        return viewHolder
    }


    override fun onBindViewHolder(holder: BinderViewHolderParent, position: Int) {
        val o = items?.get(position)
        if (o is BindingContainer) {
            holder.bindView(context, o.obj, position)
        } else {
            holder.setSelected(selectedHash != -1 && o?.hashCode() == selectedHash)
            holder.bindView(context, items?.get(position), position)
        }
//        val animation = AnimationUtils.loadAnimation(
//            context,
//            if (position > lastPosition)
//                R.anim.up_from_down
//            else
//                R.anim.down_from_up
//        )
//        holder.itemView.startAnimation(animation)
//        lastPosition = position
    }

//    override fun onViewDetachedFromWindow(holder: BinderViewHolderParent) {
//        super.onViewDetachedFromWindow(holder)
//        holder.itemView.clearAnimation()
//    }

    override fun getItemViewType(position: Int): Int {
        val o = items?.get(position)
        return (o as? BindingContainer)?.layout ?: ((o as? MultiViewType)?.itemViewType ?: 0)
    }

    override fun getItemCount(): Int {
        return items?.size!!
    }


    fun setFields(viewHolder: Class<BinderViewHolderParent>, @LayoutRes vararg layout: Int) {
        this.cls = viewHolder
        this.layout = layout
    }

    fun map(
        holder: Class<BinderViewHolderParent>,
        item: Any, @LayoutRes layout: Int
    ): MyBindingAdapter {
        items?.add(BindingContainer(holder, item, layout))
        notifyItemInserted(items?.size!! - 1)
        notifyItemRangeChanged(0, items?.size!!)
        return this
    }

    fun mapList(
        subMenuIndex: Int,
        holder: Class<BinderViewHolderParent>,
        items: MutableList<Any?>, @LayoutRes layout: Int
    ): MyBindingAdapter {
        for ((offset, item) in items.withIndex()) {
            this.items?.add(
                subMenuIndex + offset,
                BindingContainer(holder, item, layout)
            )
        }
        notifyItemRangeInserted(subMenuIndex, items.size)
        notifyItemRangeChanged(subMenuIndex, this.items?.size!!)
        return this
    }

    fun cleanAdapter(): ArrayList<*> {
        val size = items?.size!!
        for (i in 0 until size) {
            items?.removeAt(0)
        }
        this.notifyItemRangeRemoved(0, size)
        return items as ArrayList<*>
    }

    fun setSelectedHash(selectedHash: Int) {
        this.selectedHash = selectedHash
        notifyDataSetChanged()
    }

    fun removeIndividual(index: Int, offset: Int) {
        for (i in 0 until offset) {
            items?.removeAt(index)
        }
        notifyItemRangeRemoved(index, offset)
    }

}
