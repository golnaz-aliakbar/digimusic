package com.music.app.ui.activity.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.music.app.R
import com.music.app.databinding.ActivityLoginBinding
import com.music.app.di.InjectionViewModelProvider
import com.music.app.di.modules.data.pref.PreferencesManager.Companion.TOKEN
import com.music.app.helper.makeSnackbar
import com.music.app.helper.showToast
import com.music.app.ui.activity.main.ActivityMain
import com.music.app.ui.activity.main.ActivityMainViewModel
import com.music.app.ui.base.BaseActivity
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import javax.inject.Inject


class ActivityLogin : BaseActivity<ActivityLoginBinding, ActivityLoginViewModel>() {

    @Inject
    lateinit var mViewModelFactoryActivity: InjectionViewModelProvider<ActivityLoginViewModel>

    override val bindingVariable: Int? = null
    override fun getLayoutId() = R.layout.activity_login

    private val REQUEST_CODE = 1337
    private val REDIRECT_URI = "https://www.mydigipay.com/"
    private val CLIENT_ID = "5d2dec7dd0f24be0a5802682d1a5ac93"
    private val SCOPES =
        "user-read-recently-played,user-library-modify,user-read-email,user-read-private,playlist-modify-public"

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = mViewModelFactoryActivity.get(this, ActivityLoginViewModel::class)

        if(intent.getBooleanExtra(EXTRA_IS_EXPIRED,false)){

            viewModel?.getSharedPreferences()?.clearPrefs()
        }

        val token = viewModel?.getSharedPreferences()?.getString(TOKEN, null)

        if (!token.isNullOrEmpty()) {
            ActivityMain.navigate(this)
            finish()
        }


        val builder = AuthenticationRequest.Builder(
            CLIENT_ID,
            AuthenticationResponse.Type.TOKEN,
            REDIRECT_URI
        )
        builder.setScopes(arrayOf<String>(SCOPES))
        val request = builder.build()

        binding.buttonLogin.setOnClickListener {

            AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        if (requestCode == REQUEST_CODE) {
            val response = AuthenticationClient.getResponse(resultCode, intent)

            when (response.type) {
                // Response was successful and contains auth token
                AuthenticationResponse.Type.TOKEN -> {

                    showToast(this@ActivityLogin, "Login!")

                    viewModel?.getSharedPreferences()?.putString(TOKEN, response.accessToken)
                    ActivityMain.navigate(this)
                    finish()
                }

                // Auth flow returned an error
                AuthenticationResponse.Type.ERROR -> {
                    makeSnackbar(binding.root, response.error, Snackbar.LENGTH_SHORT)
                }
                else -> makeSnackbar(binding.root, "try again", Snackbar.LENGTH_SHORT)
            }
        }
    }


    companion object {
        const val EXTRA_IS_EXPIRED = "EXTRA_IS_EXPIRED"
        fun navigate(context: Context, isExpired: Boolean) {
            val i = Intent(context, ActivityLogin::class.java)
            i.putExtra(EXTRA_IS_EXPIRED,isExpired)
            context.startActivity(i)
        }
    }

}