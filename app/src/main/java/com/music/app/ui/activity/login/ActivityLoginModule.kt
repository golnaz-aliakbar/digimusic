package com.music.app.ui.activity.login

import com.music.app.di.modules.data.DataManager
import com.music.app.di.modules.data.scheduler.SchedulersFacade
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class ActivityLoginModule{
    @Provides
    fun provideViewModel(
        dataManager: DataManager,
        compositeDisposable: CompositeDisposable, schedulersFacade: SchedulersFacade
    ): ActivityLoginViewModel {
        return ActivityLoginViewModel(dataManager, compositeDisposable, schedulersFacade)
    }
}