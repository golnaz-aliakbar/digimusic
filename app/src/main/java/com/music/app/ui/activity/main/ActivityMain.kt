package com.music.app.ui.activity.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.music.app.R
import com.music.app.databinding.ActivityMainBinding
import com.music.app.di.InjectionViewModelProvider
import com.music.app.helper.progresshandler.ViewProgressHandler
import com.music.app.model.Artist
import com.music.app.ui.activity.detail.ActivityDetail
import com.music.app.ui.adapter.MyBindingAdapter
import com.music.app.ui.base.BaseActivity
import com.music.app.ui.viewholder.ItemTrackViewHolder
import ir.ucan.util.EndlessRecyclerOnScrollListener
import javax.inject.Inject

class ActivityMain : BaseActivity<ActivityMainBinding, ActivityMainViewModel>() {

    @Inject
    lateinit var mViewModelFactoryActivity: InjectionViewModelProvider<ActivityMainViewModel>

    override val bindingVariable: Int? = null
    override fun getLayoutId() = R.layout.activity_main

    private var endlessHandler: EndlessRecyclerOnScrollListener? = null
    private var adapter: MyBindingAdapter? = null
    private var tracks: ArrayList<Artist> = ArrayList()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = mViewModelFactoryActivity.get(this, ActivityMainViewModel::class)

        init()

        viewModel?.getSearch("")

        viewModel?.searchResult?.observe(this, Observer {

            tracks.clear()

            if (!it.isNullOrEmpty() && binding.editSearch.text.isNotEmpty()) {
                binding.textEmpty.visibility = View.INVISIBLE
                tracks.addAll(it)
                adapter?.notifyDataSetChanged()
            } else {
                binding.textEmpty.visibility = View.VISIBLE
                adapter?.notifyDataSetChanged()
            }
        })

        binding.editSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                tracks.clear()
                adapter?.notifyDataSetChanged()

                if (!s.isNullOrEmpty()) {
                    viewModel?.onQuery(s.toString())
                } else {
                    binding.textEmpty.visibility = View.VISIBLE
                }
            }

        })

    }

    private fun init() {

        adapter = MyBindingAdapter(
            this,
            tracks,
            ItemTrackViewHolder::class.java,
            R.layout.item_track,
            recyclerItemClickListener = { _, position ->
                tracks[position].id?.let {
                    ActivityDetail.navigate(
                        this,
                        it
                    )
                }
            }
        )

        viewModel?.loading = ViewProgressHandler(
            {
                binding.progressBar.visibility = View.VISIBLE
            },
            {
                binding.progressBar.visibility = View.INVISIBLE
            }
        )


        val layoutManager = GridLayoutManager(this, 2)
        binding.list.layoutManager = layoutManager as RecyclerView.LayoutManager?
        binding.list.adapter = adapter

//        endlessHandler = object : EndlessRecyclerOnScrollListener(layoutManager) {
//            override fun onLoadMore(page: Int) {
////                viewModel?.nextPage(page)
//            }
//        }
//        binding.list.addOnScrollListener(endlessHandler!!)
    }


    companion object {

        fun navigate(context: Context) {
            val i = Intent(context, ActivityMain::class.java)
            context.startActivity(i)
        }
    }

}
