package com.music.app.ui.adapter

interface MultiViewType {
    val itemViewType: Int
}
