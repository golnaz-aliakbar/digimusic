package com.music.app.ui.activity.detail

import androidx.lifecycle.MutableLiveData
import com.music.app.di.modules.data.DataManager
import com.music.app.di.modules.data.scheduler.SchedulersFacade
import com.music.app.model.Artist
import com.music.app.ui.base.BaseViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.HashMap

class ActivityDetailViewModel(
    dataManager: DataManager,
    compositeDisposable: CompositeDisposable,
    schedulersFacade: SchedulersFacade
) : BaseViewModel(dataManager, compositeDisposable, schedulersFacade) {

    private var disposable: HashMap<String, Disposable?> = HashMap()

    var artistResult = MutableLiveData<Artist>()

    internal fun getArtist(id: String) {
        disposable["0"]?.dispose()
        disposable["0"] =

            makeRequestForItem(
                mDataManager.networkManager.getArtistApi().getArtist(id),
                Artist::class.java,
                loading
            )
                ?.subscribe({
                    artistResult.value = it.response()?.body()

                }, Timber::e)

        addDisposable(disposable["0"]!!)
    }

}

