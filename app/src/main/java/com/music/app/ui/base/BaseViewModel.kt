package com.music.app.ui.base

import android.net.NetworkInfo
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.music.app.di.modules.data.DataManager
import com.music.app.di.modules.data.network.responseAdapter.*
import com.music.app.di.modules.data.network.utils.RxDefaultRetryHandler
import com.music.app.di.modules.data.scheduler.SchedulersFacade
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import com.music.app.helper.progresshandler.ProgressHandler
import retrofit2.adapter.rxjava2.Result
import timber.log.Timber

open class BaseViewModel(
    val mDataManager: DataManager,
    val mCompositeDisposable: CompositeDisposable,
    val mSchedulersFacade: SchedulersFacade
) : ViewModel() {

    var errorLiveData = MutableLiveData<Throwable>()
    var loading: ProgressHandler? = null

    fun <MODEL> makeRequest(
        single: Single<Result<ApiResponse>>,
        type: Class<MODEL>,
        progressHandler: ProgressHandler? = null
    ): Single<MODEL> {
        progressHandler?.showProgress()
        return mDataManager.networkManager.getNetworkConnectivityProcessor()
            .firstOrError()
            .flatMap { connectivity ->
                if (connectivity == NetworkInfo.State.CONNECTED)
                    single.flatMap(ResponseAdapter(type))
                        .subscribeOn(mSchedulersFacade.io())
                        .observeOn(mSchedulersFacade.ui())
                else
                    mDataManager.networkManager.getNetworkConnectivityProcessor()
                        .filter { t -> t == NetworkInfo.State.CONNECTED }
                        .observeOn(mSchedulersFacade.ui())
                        .firstOrError()
                        .flatMap {
                            single.flatMap(ResponseAdapter(type))
                                .subscribeOn(mSchedulersFacade.io())
                                .observeOn(mSchedulersFacade.ui())
                        }
            }
            .retry(RxDefaultRetryHandler(3))
            .doOnError(errorLiveData::setValue)
            .doFinally { progressHandler?.dismissProgress() }

    }
    fun <MODEL> makeRequestForItem(
        single: Single<Result<MODEL>>,
        type: Class<MODEL>,
        progressHandler: ProgressHandler? = null
    ): Single<Result<MODEL>>? {
        progressHandler?.showProgress()
        return mDataManager.networkManager.getNetworkConnectivityProcessor()
            .firstOrError()
            .flatMap { connectivity ->
                if (connectivity == NetworkInfo.State.CONNECTED)
                    single
                        .subscribeOn(mSchedulersFacade.io())
                        .observeOn(mSchedulersFacade.ui())
                else
                    mDataManager.networkManager.getNetworkConnectivityProcessor()
                        .filter { t -> t == NetworkInfo.State.CONNECTED }
                        .observeOn(mSchedulersFacade.ui())
                        .firstOrError()
                        .flatMap {
                            single
                                .subscribeOn(mSchedulersFacade.io())
                                .observeOn(mSchedulersFacade.ui())
                        }
            }
            .retry(RxDefaultRetryHandler(3))
            .doOnError(errorLiveData::setValue)
            .doFinally { progressHandler?.dismissProgress() }
    }

    fun addDisposable(disposable: Disposable) {
        this.mCompositeDisposable.add(disposable)
    }

    override fun onCleared() {
        this.mCompositeDisposable.dispose()
        this.mCompositeDisposable.clear()
        super.onCleared()
    }
}
