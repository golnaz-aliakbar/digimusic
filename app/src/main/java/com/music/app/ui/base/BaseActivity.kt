package com.music.app.ui.base

import android.app.Activity
import android.content.Intent
import android.net.NetworkInfo
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.music.app.di.modules.data.network.utils.RxErrorHandler
import com.music.app.ui.activity.noInternetConnection.ActivityNoInternetConnection
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.Disposable

import timber.log.Timber


abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : DaggerAppCompatActivity() {

    private var disposable: Disposable? = null

    lateinit var binding: T

    abstract val bindingVariable: Int?

    open var isHandleError = true

    open var isHandleNetworkConnectivity = true

    open var viewModel: V? = null
        set(value) {
            field = value
            bindingVariable()
            if (isHandleError) {
                field?.errorLiveData?.observe(this, Observer {
                    try {
                        RxErrorHandler(binding.root).accept(it)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
            }
        }

    @LayoutRes
    abstract fun getLayoutId(): Int


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        performDataBinding()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    public override fun onStart() {
        super.onStart()
    }

    public override fun onStop() {
        super.onStop()
    }

    override fun onResume() {
        super.onResume()

        if (isHandleNetworkConnectivity) {
            disposable = viewModel?.mDataManager?.networkManager?.getNetworkConnectivityProcessor()
                ?.subscribeOn(viewModel?.mSchedulersFacade?.io()!!)
                ?.observeOn(viewModel?.mSchedulersFacade?.ui())
                ?.subscribe({ connectivity ->
                    if (connectivity != NetworkInfo.State.CONNECTED) {
                        ActivityNoInternetConnection.navigate(this)
                    }
                }, Timber::e)
        }

    }

    override fun onPause() {
        super.onPause()
        disposable?.let {
            if (!it.isDisposed) {
                it.dispose()
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.let {
            if (!it.isDisposed) {
                it.dispose()
            }
        }
    }

    fun bindingVariable() {
        bindingVariable?.let {
            binding.setVariable(it, viewModel)
        }
        binding.executePendingBindings()
    }

    private fun performDataBinding() {
        binding = DataBindingUtil.setContentView(this, getLayoutId())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragments = supportFragmentManager.fragments
        fragments.takeIf { it.isNotEmpty() }
            ?.let { it[it.size - 1]?.onActivityResult(requestCode, resultCode, data) }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val fragments = supportFragmentManager.fragments
        fragments.takeIf { it.isNotEmpty() }?.let {
            it[it.size - 1].onRequestPermissionsResult(
                requestCode and 0xff,
                permissions,
                grantResults
            )
        }
    }

    fun setWindowFlag(activity: Activity, bits: Int, on: Boolean) {

        val win = activity.window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }
}
