package com.music.app.ui.activity.noInternetConnection

import android.content.Context
import android.content.Intent
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import com.music.app.R
import com.music.app.databinding.ActivityNoInternetConnectionBinding
import com.music.app.di.modules.data.DataManager
import com.music.app.helper.isNetworkAvailable
import com.music.app.helper.showToast
import com.music.app.ui.base.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import com.music.app.ui.base.BaseViewModel
import timber.log.Timber
import javax.inject.Inject


class ActivityNoInternetConnection :
    BaseActivity<ActivityNoInternetConnectionBinding, BaseViewModel>() {

    @Inject
    lateinit var mDataManager: DataManager

    override val bindingVariable: Int? = null

    override fun getLayoutId() = R.layout.activity_no_internet_connection

    override var isHandleNetworkConnectivity = false

    private var disposable: Disposable? = null
    private var doubleClickToExit = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.btnSetting.setOnClickListener { startActivity(Intent(Settings.ACTION_SETTINGS)) }
        binding.btnRetry.setOnClickListener {
            onResume()

            if (isNetworkAvailable(this)) {
                finish()
            }

        }
    }

    override fun onBackPressed() {
        if (doubleClickToExit) {
            finishAffinity()
            return
        }
        doubleClickToExit = true
        showToast(this, getString(R.string.twice_exit))
        Handler().postDelayed({ doubleClickToExit = false }, 2000)
    }


    override fun onResume() {
        super.onResume()
        disposable?.dispose()
        disposable = mDataManager.networkManager.getNetworkConnectivityProcessor()
            .subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ connectivity ->
                if (connectivity == NetworkInfo.State.CONNECTED) {
                    finish()
                }
            }, Timber::e)
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }

    companion object {

        fun navigate(context: Context) {
            val intent = Intent(context, ActivityNoInternetConnection::class.java)
            context.startActivity(intent)
        }
    }

}
