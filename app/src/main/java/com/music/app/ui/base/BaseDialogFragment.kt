package com.music.app.ui.base

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.music.app.di.modules.data.network.utils.RxErrorHandler
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector

import javax.inject.Inject


abstract class BaseDialogFragment<T : ViewDataBinding, V : BaseViewModel> : DialogFragment(), HasSupportFragmentInjector {

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return childFragmentInjector
    }

    lateinit var binding: T

    abstract val bindingVariable: Int?

    open var viewModel: V? = null
        set(value) {
            field = value
            bindingVariable()
            field?.errorLiveData?.observe(this, Observer {
                try {
                    RxErrorHandler(binding.root).accept(it)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
        }

    @LayoutRes
    abstract fun getLayoutId(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        dialog?.window?.setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        if (Build.VERSION.SDK_INT >= 21) {
//            val window = activity?.window
//            window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//            window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//            window?.statusBarColor = ContextCompat.getColor(activity!!, R.color.colorPrimary)
//        }

        bindingVariable?.let { binding.setVariable(it, viewModel) }
        binding.executePendingBindings()
    }

    fun bindingVariable() {
        bindingVariable?.let {
            binding.setVariable(it, viewModel)
        }
        binding.executePendingBindings()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragments = childFragmentManager.fragments
        fragments.takeIf { it.isNotEmpty() }
            ?.let { it[it.size - 1]?.onActivityResult(requestCode, resultCode, data) }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val fragments = childFragmentManager.fragments
        fragments.takeIf { it.isNotEmpty() }?.let {
            it[it.size - 1].onRequestPermissionsResult(
                requestCode and 0xff,
                permissions,
                grantResults
            )
        }
    }

}