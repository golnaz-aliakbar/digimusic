package com.music.app.ui.base

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.music.app.di.modules.data.network.utils.RxErrorHandler
import dagger.android.support.DaggerFragment

abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : DaggerFragment() {


    lateinit var binding: T

    abstract val bindingVariable: Int?

    open var viewModel: V? = null
        set(value) {
            field = value
            bindingVariable()
            field?.errorLiveData?.observe(this, Observer {
                try {
                    RxErrorHandler(binding.root).accept(it)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
        }

    @LayoutRes
    abstract fun getLayoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindingVariable?.let { binding.setVariable(it, viewModel) }
        binding.executePendingBindings()
    }

    fun bindingVariable() {
        bindingVariable?.let {
            binding.setVariable(it, viewModel)
        }
        binding.executePendingBindings()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragments = childFragmentManager.fragments
        fragments.takeIf { it.isNotEmpty() }?.let { it[it.size - 1]?.onActivityResult(requestCode, resultCode, data) }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val fragments = childFragmentManager.fragments
        fragments.takeIf { it.isNotEmpty() }?.let { it[it.size - 1].onRequestPermissionsResult(requestCode and 0xff, permissions, grantResults) }
    }

}