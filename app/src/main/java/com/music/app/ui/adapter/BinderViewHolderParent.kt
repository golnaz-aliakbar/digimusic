package com.music.app.ui.adapter

import android.content.Context
import android.view.View

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BinderViewHolderParent(protected val binding: ViewDataBinding, viewType: Int) : RecyclerView.ViewHolder(binding.root) {

    var onClickListener: ((view: View, position: Int) -> Unit)? = null
    var onLongClickListener: ((view: View, position: Int) -> Unit)? = null

    private var isSelected: Boolean = false

    var itemPosition: Int = 0

    open fun bindView(context: Context, item: Any?, position: Int) {
        this.itemPosition = adapterPosition
    }

    fun isSelected(): Boolean {
        return isSelected
    }

    open fun setSelected(selected: Boolean) {
        this.isSelected = selected
    }

    fun onClick(v: View) {
        this.onClickListener?.invoke(v, itemPosition)
    }

    fun onLongClick(v: View) {
        this.onLongClickListener?.invoke(v, itemPosition)
    }
}