package com.music.app.di.modules.data.network.interceptor

import android.content.Context
import android.os.Build
import android.text.TextUtils
import okhttp3.Interceptor
import com.music.app.BuildConfig
import com.music.app.R
import com.music.app.di.modules.data.account.AccountManager
import com.music.app.di.modules.data.pref.PreferencesManager
import com.music.app.di.modules.data.pref.PreferencesManager.Companion.TOKEN
import java.io.IOException
import java.util.regex.Pattern

class HeaderInterceptor constructor(
    val context: Context,
    val preferencesManager: PreferencesManager,
    val accountManager: AccountManager,
    val handle: Boolean = false,
    var width: Int = -1,
    var height: Int = -1
) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val original = chain.request()
        var url = original.url()
        if (!url.isHttps) {
            url = url.newBuilder().scheme("https").build()
        }
        val builder = original.newBuilder()
        val urlStr = url.toString()
        val p1 = Pattern.compile(context.getString(R.string.base_core), Pattern.CASE_INSENSITIVE)
        val p2 = Pattern.compile(context.getString(R.string.base_panel), Pattern.CASE_INSENSITIVE)
        val m1 = p1.matcher(urlStr)
        val m2 = p2.matcher(urlStr)
        if (m1.find() || m2.find()) {
            if (handle) builder.url("$url${if (urlStr.contains("?")) "&" else "?"}width=$width&height=$height")
            builder.addHeader("Content-Type", "application/json")
            preferencesManager.getString(TOKEN,null)?.takeIf { !TextUtils.isEmpty(it) }
                ?.let { builder.addHeader("Authorization", "Bearer "+it) }
        }
        val request = builder.build()
        return chain.proceed(request)
    }

}