package com.music.app.di.modules.data.database


import androidx.room.Database
import androidx.room.RoomDatabase
import com.music.app.di.modules.data.database.dao.FileDao
import com.music.app.di.modules.data.database.dao.UserDao
import com.music.app.model.DownloadModel
import com.music.app.model.User

@Database(
        entities = [User::class, DownloadModel::class],
        version = 1,
        exportSchema = false
)
abstract class DatabaseManager : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun downloadDao(): FileDao

}