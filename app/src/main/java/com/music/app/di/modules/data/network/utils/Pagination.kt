package com.music.app.di.modules.data.network.utils

import java.util.*

class Pagination : HashMap<String, String> {

    constructor() {
        pageSize = 10
        pageIndex = 0
    }

    constructor(size: Int) {
        pageSize = size
    }

    constructor(size: Int, index: Int) : this(size) {
        pageIndex = index
    }

    constructor(size: Int, index: Int, order: String, orderBy: String) : this(size, index) {
        pageOrder = order
        put("order", pageOrder.toString())

        pageOrderBy = orderBy
        put("orderBy", pageOrderBy.toString())
    }

    var pageSize: Int = 0
        set(value) {
            field = value
            put("size", pageSize.toString())
        }

    var pageIndex: Int = 0
        set(value) {
            field = value
            put("index", pageIndex.toString())
        }

    var pageOrderBy: String? = null
        set(value) {
            field = value
            put("orderBy", pageOrderBy.toString())
        }

    var pageOrder: String? = null
        set(value) {
            field = value
            put("order", pageOrder.toString())
        }

    fun clearOrder() {
        remove("orderBy")
        remove("order")
    }

    operator fun next(): Int? {
        pageIndex += 1
        return pageIndex
    }
}
