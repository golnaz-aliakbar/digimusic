package com.music.app.di.modules.data.network.routes

import com.music.app.di.modules.data.network.responseAdapter.ApiResponse
import io.reactivex.Single
import retrofit2.adapter.rxjava2.Result
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface SearchRouter {

    @GET("search")
    fun getSearchResult( @Query("q") query:String,@Query("type") type:String): Single<Result<ApiResponse>>
}