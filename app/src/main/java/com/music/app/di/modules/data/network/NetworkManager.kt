package com.music.app.di.modules.data.network


import android.net.NetworkInfo
import com.music.app.di.modules.data.network.routes.MainRouter
import com.music.app.di.modules.data.network.routes.SearchRouter
import io.reactivex.processors.BehaviorProcessor
import retrofit2.Retrofit
import javax.inject.Inject


class NetworkManager @Inject constructor(
        private var networkConnectivityProcessor: BehaviorProcessor<NetworkInfo.State>,
        private val retrofitRestApi: Retrofit
) {

    fun getNetworkConnectivityProcessor(): BehaviorProcessor<NetworkInfo.State> {
        return networkConnectivityProcessor
    }

    fun <T> create(tClass: Class<T>): T {
        return retrofitRestApi.create(tClass)
    }
    fun getSearchApi(): SearchRouter {
        return retrofitRestApi.create(SearchRouter::class.java)
    }
    fun getArtistApi(): MainRouter {
        return retrofitRestApi.create(MainRouter::class.java)
    }

}
