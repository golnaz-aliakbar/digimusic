package com.music.app.di.modules.data.database.dao


import androidx.room.*
import io.reactivex.Maybe
import io.reactivex.Single
import com.music.app.model.DownloadModel

@Dao
abstract class FileDao {

    @Query("SELECT * FROM downloadFiles")
    abstract fun all(): List<DownloadModel>

    @Query("SELECT * FROM downloadFiles WHERE contentName LIKE :contentName LIMIT 1")
    abstract fun findByName(contentName: String): DownloadModel

    @Query("SELECT * FROM downloadFiles WHERE contentId = :id LIMIT 1")
    abstract fun findById(id: Long): Maybe<DownloadModel>

    @Query("SELECT * FROM downloadFiles WHERE zoneId = :id LIMIT 1")
    abstract fun findByZoneId(id: Int): List<DownloadModel>

    @Query("SELECT * FROM downloadFiles LIMIT 1")
    abstract fun findFirstRx(): Single<DownloadModel>

    @Query("SELECT * FROM downloadFiles LIMIT 1")
    abstract fun findFirst(): DownloadModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(vararg downloadModel: DownloadModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(downloadModel: DownloadModel): Long

    @Update
    abstract fun update(downloadModel: DownloadModel)


    @Delete
    abstract fun delete(downloadModel: DownloadModel): Single<Int>

    @Query("DELETE FROM downloadFiles WHERE contentId IN (:uIds)")
    abstract fun delete(vararg uIds: Int): Int

    @Query("DELETE FROM downloadFiles")
    abstract fun deleteFirst()


    fun insertRx(downloadModel: DownloadModel): Single<Long> {
        return Single.create {
            it.onSuccess(insert(downloadModel))
        }
    }

    fun deleteRx(vararg uIds: Int): Single<Int> {
        return Single.create {
            it.onSuccess(delete(*uIds))
        }
    }
}

