package com.music.app.di.modules.data.network.responseAdapter


import io.reactivex.Completable
import io.reactivex.annotations.NonNull
import io.reactivex.functions.Function
import com.music.app.di.modules.data.network.utils.HttpException
import retrofit2.adapter.rxjava2.Result
import java.io.IOException

class MaybeResponseAdapter : Function<Result<ApiResponse>, Completable> {

    @Throws(IOException::class)
    override fun apply(@NonNull result: Result<ApiResponse>): Completable {
        return if (result.isError) {
            Completable.error(result.error()!!)
        } else {
            val response = result.response()
            val parse = response!!.body()
            if (response.isSuccessful && parse != null) {
                if (parse.isSuccessful) {
                    Completable.complete()
                } else {
                    Completable.error(
                            HttpException(
                                    parse.message ?: "", parse.statusCode
                            )
                    )
                }
            } else {
                Completable.error(
                        HttpException(
                                response.message(),
                                response.code()
                        )
                )
            }

        }
    }
}
