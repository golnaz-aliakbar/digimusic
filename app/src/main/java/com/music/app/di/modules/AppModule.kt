package com.music.app.di.modules

import com.music.app.di.modules.data.AppDataManager
import com.music.app.di.modules.data.DataManager
import com.music.app.di.modules.data.account.AccountModule
import com.music.app.di.modules.data.database.DatabaseModule
import com.music.app.di.modules.data.network.NetworkModule
import com.music.app.di.modules.data.pref.SharedPrefModule
import com.music.app.liveData.SingleLiveData
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module(includes = [
    AccountModule::class,
    NetworkModule::class,
    SharedPrefModule::class,
    DatabaseModule::class
]
)
open class AppModule {


    @Provides
    @Singleton
    fun provideDataManager(appDataManager: AppDataManager): DataManager {
        return appDataManager
    }

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

    @Provides
    fun provideIntegerSingleLiveData(): SingleLiveData<Int> {
        return SingleLiveData()
    }


}