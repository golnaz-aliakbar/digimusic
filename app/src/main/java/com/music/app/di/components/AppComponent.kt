package com.music.app.di.components

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import com.music.app.App
import com.music.app.di.modules.ActivityInjectorsModule
import com.music.app.di.modules.AppModule
import com.music.app.di.modules.FragmentInjectorsModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityInjectorsModule::class,
    FragmentInjectorsModule::class,
    AppModule::class])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(context: Context): Builder

        fun build(): AppComponent
    }

    override fun inject(app: App)

//    fun injectGlideModule(glideModule: MyAppGlideModule)

}

