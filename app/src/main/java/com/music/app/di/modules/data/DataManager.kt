package com.music.app.di.modules.data

import android.content.Context
import com.music.app.di.modules.data.account.AccountManager
import com.music.app.di.modules.data.database.DatabaseManager
import com.music.app.di.modules.data.network.NetworkManager
import com.music.app.di.modules.data.pref.PreferencesManager


interface DataManager {

    val context: Context

    val accountManager: AccountManager

    val networkManager: NetworkManager

    val preferencesManager: PreferencesManager

    val databaseManager: DatabaseManager

}
