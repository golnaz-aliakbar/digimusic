package com.music.app.di.modules.data.network.responseAdapter


import io.reactivex.Single
import io.reactivex.annotations.NonNull
import io.reactivex.functions.Function
import com.music.app.di.modules.data.network.utils.HttpException
import retrofit2.adapter.rxjava2.Result
import java.io.IOException

class MessageResponseAdapter : Function<Result<ApiResponse>, Single<Pair<Int, String>>> {

    @Throws(IOException::class)
    override fun apply(@NonNull result: Result<ApiResponse>): Single<Pair<Int, String>> {
        return if (result.isError) {
            Single.error(result.error()!!)
        } else {
            val response = result.response()
            val parse = response!!.body()
            if (response.isSuccessful && parse != null) {
//                if (parse.isSuccessful) {
                    Single.just(
                        Pair(
                            parse.statusCode,
                            parse.message ?: ""
                        )
                    )
//                } else {
//                    Single.error(
//                            HttpException(
//                                    parse.message ?: "",
//                                    parse.statusCode
//                            )
//                    )
//                }
            } else {
                Single.error(
                        HttpException(
                                response.message(),
                                response.code()
                        )
                )
            }

        }
    }
}
