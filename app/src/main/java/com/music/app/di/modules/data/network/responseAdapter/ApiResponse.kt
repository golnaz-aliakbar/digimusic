package com.music.app.di.modules.data.network.responseAdapter

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class ApiResponse {

    @JsonField(name = ["artists"])
    var artists: Any? = null

    @JsonField(name = ["Result", "result"])
    var result: Any? = null

    @JsonField(name = ["message"])
    var message: String? = null

    @JsonField(name = ["Version"])
    var version: String? = null

    @JsonField(name = ["Status"])
    var statusCode: Int = 0

    @JsonField(name = ["status"])
    var status: String? = null


    val isSuccessful: Boolean
        get() = if (status != null) status.equals("OK") else statusCode in 0..299


    constructor()

    constructor(throwable: Throwable) {
        statusCode = 500
        message = throwable.message
    }

}
