package com.music.app.di.modules.data.network.utils

class HttpException(override val message: String, val id: Int) : Exception()