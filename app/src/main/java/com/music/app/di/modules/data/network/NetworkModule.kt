package com.music.app.di.modules.data.network


import android.annotation.SuppressLint
import android.content.Context
import android.net.NetworkInfo
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.github.aurae.retrofit2.LoganSquareConverterFactory
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import dagger.Module
import dagger.Provides
import io.reactivex.processors.BehaviorProcessor
import okhttp3.OkHttpClient
import com.music.app.BuildConfig
import com.music.app.R
import com.music.app.di.modules.data.account.AccountManager
import com.music.app.di.modules.data.network.interceptor.HeaderInterceptor
import com.music.app.di.modules.data.pref.PreferencesManager
import com.music.app.di.modules.data.scheduler.SchedulersFacade
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {


    private val CACHE_CONTROL = "Cache-Control"

    @Provides
    @Singleton
    fun provideHeaderInterceptor(
        context: Context,
        preferencesManager: PreferencesManager,
        accountManager: AccountManager
    ): HeaderInterceptor {
        return HeaderInterceptor(context, preferencesManager, accountManager)
    }

    @SuppressLint("CheckResult")
    @Provides
    @Singleton
    fun providerNetworkConnectivityProcessor(context: Context, scheduler: SchedulersFacade): BehaviorProcessor<NetworkInfo.State> {

        val networkConnectivityProcessor = BehaviorProcessor.create<NetworkInfo.State>()

//        val subscribe = ReactiveNetwork.observeInternetConnectivity()
        val subscribe = ReactiveNetwork.observeNetworkConnectivity(context)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
//                .subscribe({ connect -> networkConnectivityProcessor.onNext(if (connect) NetworkInfo.State.CONNECTED else NetworkInfo.State.DISCONNECTED) }, Timber::e)
                .subscribe({ connectivity -> networkConnectivityProcessor.onNext(connectivity.state) }, Timber::e)

        return networkConnectivityProcessor
    }

    @Provides
    @Singleton
    fun provideClient(headerInterceptor: HeaderInterceptor): OkHttpClient {

        val builder = OkHttpClient.Builder()
            .connectTimeout(40, TimeUnit.SECONDS)// Set connection timeout
            .readTimeout(40, TimeUnit.SECONDS)// Read timeout
            .writeTimeout(40, TimeUnit.SECONDS)// Write timeout
                .addInterceptor(headerInterceptor)
                .cache(null)

        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(StethoInterceptor())
        }
        return builder.build()

    }
    @Provides
    @Singleton
    fun provideRestApi(context: Context, client: OkHttpClient, scheduler: SchedulersFacade): Retrofit {
        return Retrofit.Builder()
                .baseUrl(context.getString(R.string.core_url))
                .client(client)
                .addConverterFactory(LoganSquareConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(scheduler.io()))
                .build()
    }

}
