package com.music.app.di.modules.data.database.dao


import androidx.room.*
import io.reactivex.Maybe
import io.reactivex.Single
import com.music.app.model.User

@Dao
abstract class UserDao {

    @Query("SELECT * FROM users")
    abstract fun all(): List<User>

    @Query("SELECT * FROM users WHERE firstName LIKE :firstName LIMIT 1")
    abstract fun findByName(firstName: String): User

    @Query("SELECT * FROM users WHERE id = :id LIMIT 1")
    abstract fun findById(id: Long): Maybe<User>

    @Query("SELECT * FROM users LIMIT 1")
    abstract fun findFirstRx(): Single<User>

    @Query("SELECT * FROM users LIMIT 1")
    abstract fun findFirst(): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(vararg user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(user: User): Long

    @Update
    abstract fun update(user: User)


    @Delete
    abstract fun delete(user: User): Single<Int>

    @Query("DELETE FROM users WHERE id IN (:uIds)")
    abstract fun delete(vararg uIds: Int): Int

    @Query("DELETE FROM users")
    abstract fun deleteFirst()


    fun insertRx(user: User): Single<Long> {
        return Single.create {
            it.onSuccess(insert(user))
        }
    }

    fun deleteRx(vararg uIds: Int): Single<Int> {
        return Single.create {
            it.onSuccess(delete(*uIds))
        }
    }
}

