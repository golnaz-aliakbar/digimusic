package com.music.app.di.modules.data.network.responseAdapter

import com.bluelinelabs.logansquare.annotation.JsonField
import com.bluelinelabs.logansquare.annotation.JsonObject

@JsonObject
class ApiResponseGeneric<MODEL>(

    var result: MODEL? = null,

    @JsonField(name = ["Message"])
    var message: String? = null,

    @JsonField(name = ["Status"])
    var statusCode: Int = 0,

    @JsonField(name = ["status"])
    var status: String? = null

) {

    val isSuccessful: Boolean
        get() = if (status != null) status.equals("OK") else (statusCode in 200..299) || (statusCode == 1)
}