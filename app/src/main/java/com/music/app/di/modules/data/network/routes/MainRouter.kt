package com.music.app.di.modules.data.network.routes

import com.music.app.di.modules.data.network.responseAdapter.ApiResponse
import com.music.app.model.Artist
import io.reactivex.Single
import retrofit2.adapter.rxjava2.Result
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface MainRouter {

    @GET("artists/{id}")
    fun getArtist(@Path("id") id:String): Single<Result<Artist>>


}
