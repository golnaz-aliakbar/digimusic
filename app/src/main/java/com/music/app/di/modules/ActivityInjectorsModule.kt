package com.music.app.di.modules


import com.music.app.ui.activity.detail.ActivityDetail
import com.music.app.ui.activity.detail.ActivityDetailModule
import com.music.app.ui.activity.login.ActivityLogin
import com.music.app.ui.activity.login.ActivityLoginModule
import com.music.app.ui.activity.main.ActivityMain
import com.music.app.ui.activity.main.ActivityMainModule
import com.music.app.ui.activity.noInternetConnection.ActivityNoInternetConnection
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityInjectorsModule {

    @ContributesAndroidInjector(modules = [ActivityLoginModule::class])
    abstract fun activityLoginInjector(): ActivityLogin

    @ContributesAndroidInjector(modules = [ActivityMainModule::class])
    abstract fun activityMainInjector(): ActivityMain

    @ContributesAndroidInjector(modules = [ActivityDetailModule::class])
    abstract fun activityDetailInjector(): ActivityDetail

    @ContributesAndroidInjector()
    abstract fun activityNoInternetConnectionInjector(): ActivityNoInternetConnection
}