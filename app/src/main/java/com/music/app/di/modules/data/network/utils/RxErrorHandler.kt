package com.music.app.di.modules.data.network.utils


import android.app.Activity
import android.view.View
import com.google.android.material.snackbar.Snackbar
import io.reactivex.functions.Consumer
import com.music.app.R
import com.music.app.helper.makeSnackbar
import com.music.app.ui.activity.login.ActivityLogin

import java.nio.charset.Charset


class RxErrorHandler(internal var view: View) : Consumer<Throwable> {

    private fun isValidISOLatin1(s: String): Boolean {
        return Charset.forName("US-ASCII").newEncoder().canEncode(s)
    }

    @Throws(Exception::class)
    override fun accept(throwable: Throwable?) {
        if (throwable == null) {
            return
        }
        when {
            throwable is HttpException -> {

                if (throwable.id == 401) {
                    val context = view.context

                    ActivityLogin.navigate(context,true)
                    (context as Activity).finish()
                } else {
                    throwable.message.let {
                        makeSnackbar(
                                view,
                                it,
                                Snackbar.LENGTH_LONG
                        ).show()
                    }
                }
            }
            throwable is NullPointerException -> {
                throwable.message?.let {
                    makeSnackbar(view, it, Snackbar.LENGTH_LONG).show()
                }
            }
            throwable.message != null -> {
                throwable.message?.let {
                    val string: String = if (isValidISOLatin1(it)) {
                        view.context.getString(R.string.failed_connect)
                    } else {
                        it
                    }
                    makeSnackbar(view, string, Snackbar.LENGTH_LONG).show()

                }
            }
        }
        throwable.printStackTrace()
    }
}

