package com.music.app.di.modules.data.network.responseAdapter


import com.bluelinelabs.logansquare.LoganSquare
import io.reactivex.Single
import io.reactivex.annotations.NonNull
import io.reactivex.functions.Function
import com.music.app.di.modules.data.network.utils.HttpException
import retrofit2.adapter.rxjava2.Result
import java.io.IOException


class ResponseAdapter<T>(private val tClass: Class<T>) : Function<Result<ApiResponse>, Single<T>> {

    @Throws(IOException::class)
    override fun apply(@NonNull result: Result<ApiResponse>): Single<T> {
        return if (result.isError) {
            Single.error(result.error())
        } else {
            val response = result.response()
            val parse = response!!.body()
            if (response.isSuccessful && parse != null) {
                if (parse.isSuccessful) {
//                    val map = parse.result as Map<*, *>
//                    if (map.contains("Data")) {
//                        Single.just(LoganSquare.parse(LoganSquare.serialize(map["Data"]), tClass))
//                    } else {
//                        Single.just(LoganSquare.parse(LoganSquare.serialize(parse.result), tClass))
//                    }

                    Single.just(LoganSquare.parse(LoganSquare.serialize(parse.artists), tClass))
                } else {
                    Single.error(
                        HttpException(
                            parse.message ?: "",
                            parse.statusCode
                        )
                    )
                }
            } else {
                Single.error(
                    HttpException(
                        response.message(),
                        response.code()
                    )
                )
            }
        }
    }
}
