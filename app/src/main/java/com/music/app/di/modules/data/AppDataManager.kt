package com.music.app.di.modules.data

import android.content.Context
import com.music.app.di.modules.data.account.AccountManager
import com.music.app.di.modules.data.database.DatabaseManager
import com.music.app.di.modules.data.network.NetworkManager
import com.music.app.di.modules.data.pref.PreferencesManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDataManager
@Inject
constructor(
    override val context: Context,
    override val accountManager: AccountManager,
    override val networkManager: NetworkManager,
    override val preferencesManager: PreferencesManager,
    override val databaseManager: DatabaseManager
) : DataManager


