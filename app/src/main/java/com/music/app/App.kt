package com.music.app

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.stetho.Stetho
import com.music.app.di.components.DaggerAppComponent
import com.music.app.helper.CrashReportingTree
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber


class App : DaggerApplication() {


    @SuppressLint("CheckResult")
    override fun onCreate() {
        super.onCreate()


        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        initStetho()
        initTimber()



    }
    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }
    private fun initTimber() {
        Timber.plant(if (BuildConfig.DEBUG)
            Timber.DebugTree()
        else {
            CrashReportingTree()
        })
    }

//    override fun attachBaseContext(context: Context) {
//        super.attachBaseContext(context)
//        MultiDex.install(this)
//    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerAppComponent.builder().application(this.applicationContext).build()
        component.inject(this)
        return component
    }

}
