package com.music.app.helper

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.music.app.R
import jp.wasabeef.glide.transformations.CropSquareTransformation


@BindingAdapter("imageResource")
fun setImageResource(imageView: ImageView, resource: Int) {
    if (resource != -1) {
        imageView.setImageResource(resource)
    }
}

@BindingAdapter("imageResource")
fun setImageResource(imageView: ImageView, drawable: Drawable?) {
    imageView.setImageDrawable(drawable)
}

@BindingAdapter("imageFit")
fun loadImageFit(imageView: ImageView, url: String?) {
    Glide.with(imageView.context)
        .load(url)
        .fitCenter()
        .into(imageView)
}

@BindingAdapter("imageCenterCrop")
fun loadImageCenterCrop(imageView: ImageView, url: String?) {
    Glide
        .with(imageView.context)
//            .load("$url?mode=crop")
        .load(url)
        .apply(RequestOptions.centerCropTransform())
        .into(imageView)
}


@BindingAdapter("roundContent")
fun roundContent(imageView: ImageView, url: String?) {
    Glide.with(imageView)
        .load(url)
        .apply(RequestOptions.bitmapTransform(RoundedCorners(dpToPx(10))).centerCrop())
        .into(imageView)
}

@BindingAdapter("imageCrop")
fun loadImageCrop(imageView: ImageView, url: String?) {
    Glide
        .with(imageView.context)
        .load(url)
//        .placeholder(R.drawable.placeholder)
        .apply(RequestOptions.centerCropTransform())
        .into(imageView)
}

@BindingAdapter("imageSquare")
fun loadImageSquare(imageView: ImageView, url: String?) {
    Glide
        .with(imageView.context)
        .load(url)
        .apply(
            RequestOptions().transforms(CenterCrop(), RoundedCorners(dpToPx(8)))
        )
        .into(imageView)
}

@BindingAdapter("imageSquareCrop")
fun loadImageSquareCrop(imageView: ImageView, url: String?) {
    Glide
        .with(imageView.context)
        .load("$url?mode=crop")
        .apply(
            RequestOptions().transforms(CenterCrop(), RoundedCorners(dpToPx(8)))
        )
        .into(imageView)
}

@BindingAdapter("imageFitWithPlaceholder", "placeholder")
fun loadImageFitWithPlaceholder(imageView: ImageView, url: String?, placeholder: Drawable?) {
    Glide
        .with(imageView.context)
        .load(url)
        .fitCenter()
        .placeholder(placeholder)
        .into(imageView)
}

@BindingAdapter("imageCircleWithPlaceholder", "placeholder")
fun loadImageCircleWithPlaceholder(imageView: ImageView, url: String?, placeholder: Drawable?) {
    Glide
        .with(imageView.context)
        .load(url)
        .apply(RequestOptions.bitmapTransform(CircleCrop()))
        .placeholder(placeholder)
        .into(imageView)
}

@BindingAdapter("imageSquareWithPlaceHolder", "placeholder")
fun loadImageSquareWithPlaceHolder(imageView: ImageView, url: String?, placeholder: Drawable?) {
    Glide
        .with(imageView.context)
        .load(url)
        .placeholder(placeholder)
        .apply(RequestOptions.bitmapTransform(CropSquareTransformation()))
        .into(imageView)
}

@BindingAdapter("imageRoundedCorner", "imageRoundSize")
fun getImageRoundedCorner(imageView: ImageView, url: String?, roundCorner: Int) {
    Glide.with(imageView)
        .load(url)
        .apply(
            RequestOptions()
                .transforms(CenterCrop(), RoundedCorners(dpToPx(roundCorner)))
//                .placeholder(R.drawable.placeholder)
        )
        .into(imageView)
}

