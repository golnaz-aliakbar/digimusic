package com.music.app.helper

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.Settings
import android.view.View
import com.music.app.di.modules.data.pref.PreferencesManager
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

fun getUUID(context: Context): String {
    return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
}

fun getFormattedPersianDate(year: Int, month: Int, day: Int): String {
    return year.toString() + "-" + (1 + month) + "-" + day
}

fun enablePermission(context: Context) {
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    val uri = Uri.fromParts("package", context.packageName, null)
    intent.data = uri
    context.startActivity(intent)
}

fun randomString(length: Int): String {
    val charPool: List<Char> =
        ('a'..'z') + ('A'..'Z') + ('0'..'9') + '!' + '@' + '#' + '$' + '%' + '&' + '*' + '(' + ')'
    return (1..length)
        .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
        .map(charPool::get)
        .joinToString("")
}

fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}

fun setMoneySeparator(price: Int): String {
    val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
    val symbols = formatter.decimalFormatSymbols

    symbols.groupingSeparator = ','
    formatter.decimalFormatSymbols = symbols
    return formatter.format(price.toLong())
}

fun getActivity(v: View): Activity? {
    var context = v.context
    while (context is ContextWrapper) {
        if (context is Activity) {
            return context
        }
        context = context.baseContext
    }
    return null
}
fun trimTrailingWhitespace(source: CharSequence?): CharSequence {

    if (source == null)
        return ""

    var i = source.length

    // loop back to the first non-whitespace character
    while (--i >= 0 && Character.isWhitespace(source[i])) {
    }

    return source.subSequence(0, i + 1)
}
fun getStringSizeLengthFile(size: Long): String {

    val df = DecimalFormat("0.00")

    val sizeKb = 1024.0f
    val sizeMo = sizeKb * sizeKb
    val sizeGo = sizeMo * sizeKb
    val sizeTerra = sizeGo * sizeKb


    if (size < sizeMo)
        return df.format((size / sizeKb).toDouble()) + " KB"
    else if (size < sizeGo)
        return df.format((size / sizeMo).toDouble()) + " MB"
    else if (size < sizeTerra)
        return df.format((size / sizeGo).toDouble()) + " GB"

    return ""
}

