package com.music.app.helper

import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.music.app.R

fun makeSnackbarCenter(view: View, @StringRes message: Int, duration: Int): Snackbar {
    return makeSnackbarCenter(
            view,
            view.context.resources.getString(message),
            duration
    )
}
@JvmOverloads
fun show(view: View, text: String, @BaseTransientBottomBar.Duration duration: Int = Snackbar.LENGTH_SHORT, callback: Snackbar.Callback? = null) {
    val make = Snackbar.make(view, text, duration)
    val tv = make.view.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView

    if (callback != null)
        make.addCallback(callback)

    make.show()
}

@JvmOverloads
fun showAction(view: View, text: String, action : String, clickListener: View.OnClickListener
               , @BaseTransientBottomBar.Duration duration: Int = Snackbar.LENGTH_SHORT
               , callback: Snackbar.Callback? = null) {


    val make = Snackbar.make(view, text, duration)
    val tv = make.view.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView


    if (callback != null)
        make.addCallback(callback)

    make.setAction(action, clickListener)

    make.show()
}

fun makeSnackbarCenter(view: View, message: String, duration: Int): Snackbar {
    val snackbar = makeSnackbar(view, message, duration)
    val tv = snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
    tv.gravity = Gravity.CENTER_HORIZONTAL
    return snackbar
}

fun makeSnackbar(view: View, @StringRes message: Int, duration: Int): Snackbar {
    return makeSnackbar(view, view.context.resources.getString(message), duration)
}

fun makeSnackbar(view: View, message: String, duration: Int): Snackbar {
    val snackbar = Snackbar.make(view, message, duration)
    val tv = snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
    val layoutParams = snackbar.view.layoutParams as FrameLayout.LayoutParams
    layoutParams.gravity = Gravity.TOP
    layoutParams.topMargin = dpToPx(44)
    layoutParams.marginStart = dpToPx(8)
    layoutParams.marginEnd = dpToPx(8)
    snackbar.view.layoutParams = layoutParams
    snackbar.view.setBackgroundColor(ContextCompat.getColor(view.context, R.color.colorDivider))
    tv.textAlignment = View.TEXT_ALIGNMENT_CENTER
    tv.gravity = Gravity.CENTER_HORIZONTAL
    tv.setTextColor(ContextCompat.getColor(view.context, R.color.colorBlack))
    return snackbar
}