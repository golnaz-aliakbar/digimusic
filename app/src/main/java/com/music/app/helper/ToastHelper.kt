package com.music.app.helper

import android.content.Context
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IntDef
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.music.app.R


@IntDef(
        NORAML,
        ERROR,
        WARNING,
        INFO,
        SUCCESS
)
@Retention(AnnotationRetention.SOURCE)
annotation class ToastyTypeNames

const val NORAML = 0
const val ERROR = 1
const val WARNING = 2
const val INFO = 3
const val SUCCESS = 4

@IntDef(LENGTH_SHORT, LENGTH_LONG)
@Retention(AnnotationRetention.SOURCE)
annotation class ToastyDurationMode

const val LENGTH_SHORT = 0
const val LENGTH_LONG = 1

fun showToast(context: Context, message: String) {
    val toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
    val view = toast.view
//    view.setBackgroundResource(R.drawable.bg_toast)
    val textView = view.findViewById<TextView>(android.R.id.message)
    textView.textSize = 14f
    textView.setPadding(dpToPx(16), 0, dpToPx(16), 0)
    textView.setTextColor(ContextCompat.getColor(context, R.color.colorDivider))
    toast.show()
}


fun showToast(context: Context, message: String, @ToastyTypeNames type: Int, @ToastyDurationMode duration: Int) {

//    when (type) {
//        Normal -> Toasty.normal(context, message, duration).show()
//        Error -> Toasty.error(context, message, duration).show()
//        Info -> Toasty.info(context, message, duration).show()
//        Success -> Toasty.success(context, message, duration).show()
//        Warning -> Toasty.warning(context, message, duration).show()
//    }
}

fun showToast(context: Context, @StringRes string: Int, @ToastyTypeNames type: Int, @ToastyDurationMode duration: Int) {
//    showToast(context.getString(string), type, duration)
}



