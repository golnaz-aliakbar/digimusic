package com.music.app.helper.progresshandler

interface ProgressHandler {
    fun showProgress()

    fun dismissProgress()
}
