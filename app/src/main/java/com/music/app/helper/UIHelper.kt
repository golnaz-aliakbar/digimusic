package com.music.app.helper

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.res.Resources
import android.graphics.Point
import android.os.Build
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ScrollView


import java.util.*



fun showKeyboardInDialog(dialog: Dialog?, target: View?) {
    if (dialog == null || target == null) {
        return
    }
    dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    target.requestFocus()
}

fun showKeyboard(context: Context?, target: View?) {
    if (context == null || target == null) {
        return
    }
    getInputMethodManager(context).showSoftInput(target, InputMethodManager.SHOW_IMPLICIT)
}

fun hideKeyboard(context: Context?, target: View?) {
    if (context == null || target == null) {
        return
    }
    getInputMethodManager(context).hideSoftInputFromWindow(target.windowToken, 0)
}

fun hideKeyboard(activity: Activity) {
    hideKeyboard(activity, activity.window.decorView)
}

fun showKeyboard(activity: Activity) {
    showKeyboard(activity, activity.currentFocus)
}

private fun getInputMethodManager(context: Context): InputMethodManager {
    return context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
}

fun scrollToView(scrollViewParent: ScrollView, view: View) {
    val childOffset = Point()
    getDeepChildOffset(scrollViewParent, view.parent, view, childOffset)
    scrollViewParent.smoothScrollTo(0, childOffset.y)
}

private fun getDeepChildOffset(mainParent: ViewGroup, parent: ViewParent, child: View, accumulatedOffset: Point) {
    val parentGroup = parent as ViewGroup
    accumulatedOffset.x += child.left
    accumulatedOffset.y += child.top
    if (parentGroup == mainParent) {
        return
    }
    getDeepChildOffset(mainParent, parentGroup.parent, parentGroup, accumulatedOffset)
}

fun setLayoutDirection(context: Context, isEnglish: Boolean) {
    val dm = context.resources.displayMetrics
    val config = context.resources.configuration
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        config.setLocale(if (isEnglish) Locale.US else Locale("fa"))
        context.resources.updateConfiguration(config, dm)
    }
}

fun pxToDp(px: Int): Int {
    return (px / Resources.getSystem().displayMetrics.density).toInt()
}

fun dpToPx(dp: Int): Int {
    return (dp * Resources.getSystem().displayMetrics.density).toInt()
}

fun spToPx(sp: Float): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, Resources.getSystem().displayMetrics).toInt()
}

fun getScreenWidth(context: Context): Int {
    val dm = DisplayMetrics()
    (context as Activity).windowManager.defaultDisplay.getMetrics(dm)
    return dm.widthPixels
}

fun getScreenHeight(context: Context): Int {
    val dm = DisplayMetrics()
    (context as Activity).windowManager.defaultDisplay.getMetrics(dm)
    return dm.heightPixels
}

